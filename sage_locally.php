<?php

namespace Deployer;

// Set default path of distribution folder. This is /public with Sage 10,
// and /dist with Sage 9
set('sage/dist_path', '/public');

// Build script used. Will be "build" with Sage 10 or "build:production" with Sage 9
set('sage/build_command', 'build');

desc( 'Runs composer install on remote server' );
task( 'sage:vendors', function () {
    run( 'cd {{release_path}}/{{theme_path}} && {{bin/composer}} {{composer_options}}' );
} );

desc( 'Compiles the theme locally for production' );
task( 'sage:compile:locally', function () {
    runLocally( "cd {{local_root}}/{{theme_path}} && yarn run {{sage/build_command}}" );
} );

desc( 'Removes the folder for distributed files on the destination' );
task( 'sage:clear_assets', function () {
    run( 'rm -rf {{release_path}}/{{theme_path}}{{sage/dist_path}}' );
} );

desc( 'Updates remote assets with local assets, but without deleting previous assets on destination' );
task( 'sage:upload_assets_only', function () {
    upload( '{{local_root}}/{{theme_path}}{{sage/dist_path}}', '{{release_path}}/{{theme_path}}' );
} );

desc( 'Updates remote assets with local assets' );
task( 'sage:upload_assets', [
    'sage:clear_assets',
    'sage:upload_assets_only',
] );

desc( 'Builds assets and uploads them on remote server' );
task( 'push:assets', [
    'sage:compile',
    'sage:upload_assets',
] );


desc( 'Uploads nodes_modules');
task ( 'modules:upload', function () {
    upload( '{{local_root}}/{{theme_path}}/node_modules', '{{release_path}}/{{theme_path}}' );
} );

desc( 'Remove nodes_modules folder from previous release');
task ( 'clean:modules', function () {
    $releases = get('releases_list');
    $keepReleases = get( 'keep_releases');

    if ($keepReleases === 1) {
        echo "\033[0;33mNo previous release existing, nothing to remove\n";
    } else {
        run( "rm -rf {{deploy_path}}/releases/{$releases[1]}/{{theme_path}}/node_modules");
    }
} );
