<?php

namespace Deployer;

// Backup database
set('option_export', '');
set('bin/wp', 'wp');

desc('Backup BDD');
task('backup:bdd', function () {

    $checkCurrent = run("test -L {{deploy_path}}/current && echo '0' || echo '1' ");
    $backupPath = get('backup_path');

    if($checkCurrent == 0) {
   	$exportFilename = get('application').'.' . date('Y-m-d-H:i') . '.sql';
   	$exportAbsPath  = get('backup_path').'/database';
   	$exportAbsFile  = $exportAbsPath . '/' . $exportFilename;

   	run("mkdir --parents  {$exportAbsPath}");
   	run("cd {{current_path}} && {{bin/wp}} db export {{option_export}} {$exportAbsFile}");
   	run("gzip {$exportAbsFile}");
	run("find {$exportAbsPath} -name \"{{application}}.????-??-??-??:??.sql.gz\" | sort | head --lines=\"-10\" | xargs rm --force");

	echo "\033[0;32mDump backup success in \033[0;34m{$backupPath}\n";
    } else {
	echo "\033[0;33mNo previous release existing, can't backup\n";
    }
} );
