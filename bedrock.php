<?php

namespace Deployer;

desc( 'Installing Bedrock vendors' );
task( 'bedrock:vendors', function () {
    run( 'cd {{release_path}} && {{bin/composer}} {{composer_options}}' );
} );
