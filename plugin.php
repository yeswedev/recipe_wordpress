<?php

namespace Deployer;

// Manage plugins
set('option_plugin_deactivate', '--all');
set('option_plugin_activate', '--all');
set('bin/wp', 'wp');

desc('Deactivate plugin before deploy');
task('plugin:deactivate', function () {
	run("cd {{release_path}} && {{bin/wp}} plugin deactivate {{option_plugin_deactivate}}");
	$status = run("cd {{release_path}} && {{bin/wp}} plugin status");
	echo "{$status}\n";
} );

desc('Activate plugin after deploy');
task('plugin:activate', function () {
	run("cd {{release_path}} && {{bin/wp}} plugin activate {{option_plugin_activate}}");
	$status = run("cd {{release_path}} && {{bin/wp}} plugin status");
	echo "{$status}\n";
} );

// str_contains need PHP8.0 For earlier versions of PHP, you can polyfill the str_contains function using the following snippet:
if (!function_exists('str_contains')) {
    function str_contains($haystack, $needle) : bool {
        return $needle !== '' && mb_strpos($haystack, $needle) !== false;
    }
}

desc('Clean WPRocket cache');
task('rocket:clean', function () {
    $status = run("cd {{release_path}} && {{bin/wp}} plugin status wp-rocket");
    if(str_contains($status,'Status: Active')){
        run("cd {{release_path}} && {{bin/wp}} rocket clean --confirm");
    }
} );
