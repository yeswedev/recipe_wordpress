<?php

namespace Deployer;

// Activate maintenance
set('bin/wp', 'wp');

desc('Activate maintenance mode before deploy');
task('maintenance:activate', function () {
        run("cd {{release_path}} && {{bin/wp}} maintenance-mode activate");
        $status = run("cd {{release_path}} && {{bin/wp}} maintenance-mode status");
        echo "\033[0;31m{$status}\033[0m\n";
} );

// Deactivate maintenance
desc('Deactivate maintenance mode  after deploy');
task('maintenance:deactivate', function () {
        run("cd {{deploy_path}}/current && {{bin/wp}} maintenance-mode deactivate");
        $status = run("cd {{deploy_path}}/current && {{bin/wp}} maintenance-mode status");
        echo "\033[0;36m{$status}\033[0m\n";
} );
