<?php

namespace Deployer;

set('node_bin', '');
set('node_builder', 'yarn');
set('node_script', 'build');

desc( 'Compile the theme on server' );
task( 'sage:compile:server', function () {
   run( 'export PATH={{node_bin}}:$PATH && echo $PATH {{node_bin}}  && cd {{release_path}}/{{theme_path}} && {{node_bin}}/{{node_builder}} install' );
   run( 'export PATH={{node_bin}}:$PATH && echo $PATH {{node_bin}}  && cd {{release_path}}/{{theme_path}} && {{node_bin}}/{{node_builder}} run {{node_script}}' );
} );
