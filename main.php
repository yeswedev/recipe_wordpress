<?php

namespace Deployer;

require 'fpm.php';
require 'backup.php';
require 'bedrock.php';
require 'sage_locally.php';
require 'sage_server.php';
require 'plugin.php';
require 'maintenance.php';

desc( 'Begin deployement with most common task' );
task( 'common:start', [
	'deploy:info',
	'deploy:prepare',
       	'deploy:lock',
	'backup:bdd',
	'deploy:release',
	'deploy:update_code',
	'deploy:shared',
	'deploy:writable',
	'deploy:vendors',
] );

desc ( 'Common vendor task' );
task( 'vendors:install', [
	'bedrock:vendors',
	'sage:vendors'
] );

desc ( 'Bedrock assets on server from FlorianMoser' );
task ( 'sage:assets:server', [
	'plugin:deactivate',
	'sage:compile:server',
	'clean:modules'
] );

desc ( 'Bedrock assets locally and upload' );
task ( 'sage:assets:locally', [
        'sage:compile:locally',
	'sage:upload_assets_only',
	'modules:upload',
	'clean:modules'
] );

desc ( 'Create symbolic links task' );
task ( 'create:symlink', [
	'deploy:clear_paths',
        'deploy:symlink'
] );

desc ( 'Reload fpm task' );
task ( 'fpm:reload', [
        'php:opcache_reset',
        'php:clearstatcache'
] );

desc ( 'End deployement with common task' );
task ( 'common:end', [
	'plugin:activate',
        'deploy:unlock',
        'cleanup',
        'success'
] );
