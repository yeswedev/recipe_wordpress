<?php

namespace Deployer;

set('curl_options', '');
set('public_folder', 'web');

set('opcache_reset_url', 'opcache_reset');

desc('Opcache reset');
task('php:opcache_reset', function () {
    $releases = get('releases_list');
    if (!has('previous_release')) {
        echo "\033[0;33mNo previous release existing\n";
    } else {
        run('echo "<?php opcache_reset(); ?>" > {{deploy_path}}/releases/'.$releases[1].'/{{public_folder}}/{{opcache_reset_url}}.php');
        run('echo "<?php opcache_reset(); ?>" > {{release_path}}/{{public_folder}}/{{opcache_reset_url}}.php');
        run('curl {{curl_options}} {{url}}/{{opcache_reset_url}}.php');
        run('rm {{release_path}}/{{public_folder}}/{{opcache_reset_url}}.php');
    }
});

set('clearstatcache_url', 'clearstatcache');

desc('Clear Stat Cache');
task('php:clearstatcache', function () {
    $releases = get('releases_list');
    if (!has('previous_release')) {
        echo "\033[0;33mNo previous release existing\n";
    } else {
        run('echo "<?php clearstatcache(); ?>" > {{deploy_path}}/releases/'.$releases[1].'/{{public_folder}}/{{clearstatcache_url}}.php');
        run('echo "<?php opcache_reset(); ?>" > {{release_path}}/{{public_folder}}/{{clearstatcache_url}}.php');
        run('curl {{curl_options}} {{url}}/{{clearstatcache_url}}.php');
        run('rm {{release_path}}/{{public_folder}}/{{clearstatcache_url}}.php');
    }
});
