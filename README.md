## **Template deploy.php**

**---**

```php
require 'recipe/common.php';  
require './recipe/main.php';  
```

**---**

#### <span style="color: #e63946">_Add missing shared files/dir_</span>  

```php
set('shared_dirs', ['web/app/uploads', 'web/app/cache']);  
set('shared_files', ['.env','web/.htaccess']);  
```



#### <span style="color: #e63946">_Options you can add_</span>  

```php
set('bin/wp', ' ');  // if different from wp
set('option_export', ' ');  // if specific plugins
set('option_plugin', ' '); // if specific plugins
set('node_bin', ' '); // if assets:server
```



#### <span style="color: #e63946">_Variables to define in hosts_</span>  

```php
set('url', '');  // reload for php fpm
set('curl_options', '');  // if htacess enable
set('public_folder', ''); // if different from web 
set('backup_path', ''); // create specific backup path for hosts  
```



#### <span style="color: #e63946">_Complete if you need specific task_</span>  

```php
desc('Custom task if special dependances');  
task('custom:task', [  
] );  
```

**---**

#### <span style="color: #2a9d8f">_Deploy tasks_</span>  

```php
desc('Deploy your project');  
task('deploy', [  
    'common:start',
    'sage:assets:server' or 'sage:assets:locally',
    'custom:task', 
    'create:symlink',  
    'fpm:reload',  
    'common:end'  
] );
```

